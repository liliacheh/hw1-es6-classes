class Employee {
    constructor(name = '', age = 18, salary = 0) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name
    }
    set name(newName) {
        this._name = newName
    }
    get age() {
        return this.age
    }
    set age(newAge) {
        !isNaN(newAge) && newAge >= 18 ? this._age = newAge : alert('write correct age')
    }
    get salary() {
        return this._salary
    }
    set salary(newSalary) {
        !isNaN(newSalary) && newSalary > 0 ? this._salary = newSalary : alert('write correct salary')
    }
}
class Programmer extends Employee {
    constructor(name = '', age = 18, salary = 0, lang = []) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return super.salary * 3
    }
    set salary(newSalary) {
        super.salary = newSalary
    }
    get lang() {
        return this._lang
    }
    set lang(newLang) {
        if (typeof newLang === 'string' && newLang.length > 1 && isNaN(newLang)) {
            this._lang.push(newLang)
        } else alert('write correct language')
    }
}
const programmerMark = new Programmer('Mark', 21, 500, ['ua', 'ru']);
const programmerEmily = new Programmer('Emily', 25, 800, ['ua', 'ru', 'eng']);
const programmerMarta = new Programmer('Marta', 33, 3000, ['ua', 'ru', 'eng', 'es']);

console.log(programmerMark);
console.log(programmerEmily);
console.log(programmerMarta);


