1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
   *******
   У прототипі зберігаються методи, які можуть використовувати нащадки. Тобто, наприклад, є об'єкт зі своїми методами і властивостями. І ми хочемо створити новий об'єкт чи декілька, які використовують методи початкового,але мають ще якісь свої методи. Тоді нові об'єкти створені на базі початкового, будуть мати доступ до методів батьківського.
   *******
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
   *******
   Спершу у конструкторі класу-нащадка запускається батьківський конструктор, і лише потім власний. Тому спершу потрібно викликати super(), щоб батьківський конструктор спрацював.